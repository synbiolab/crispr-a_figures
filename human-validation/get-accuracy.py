import numpy as np
import pandas as pd

sample_list = ["SRR3699730", "SRR3699749", "SRR3699748", "SRR3699731", "SRR3699703", "SRR3699736", "SRR3699771", "SRR3699778", "SRR3699758", "SRR3699789"]
selected_sample = sample_list[8]

### Human validated results
sample_results = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/"+ selected_sample + "_examinated.csv")
### Indels: edits and errors
initial_edits = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/CRISPR-A_results/"+ selected_sample + "_0.1_indels.csv")
final_edits = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/CRISPR-A_results/"+ selected_sample + "_0.1_CorrectedIndels.csv")
### Wt and discarted
as_wt = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/CRISPR-A_results/"+ selected_sample + "_0.1_wt-reads.csv")
as_wt_discarted = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/CRISPR-A_results/"+ selected_sample + "_0.1_incorrect-wt-reads.csv")
as_aln_discarted = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/CRISPR-A_results/"+ selected_sample + "_0.1_truncated-reads.csv")

##### Decleare counts 
n_total = 0
n_wt_wt = 0 
n_truncated = 0
n_wt_discart = 0
n_discarted = 0 ## discarded edits
n_discarted_reads = 0
# Classification count
n_tn = 0
n_tp = 0
n_fp = 0
n_fn = 0

####### Check classes
print("Total number of validated reads:")
print(sample_results.shape[0])
print("Used classes in the classification:")
print(set(sample_results.Classify))

# Total number of analayzed reads
n_total = n_total + sample_results.shape[0] 

### Wt (by human assesmeny) classified as wt (by CRISPR-A)
print("wt as wt!!! (or not...)")
wt_wt = sample_results.loc[(sample_results.Classify == "wt") & (sample_results.Nomenclature == "wt")]
n_wt_wt = n_wt_wt + wt_wt.shape[0] 
print(n_tn)
checking_tp = wt_wt.loc[ wt_wt["Id"].isin( list(as_wt["V1"]) ) ]
n_tn = n_tn + checking_tp.shape[0]
print(n_tn)
truncated = wt_wt.loc[ wt_wt["Id"].isin( list(as_aln_discarted["V1"]) ) ]
print(n_truncated)
n_truncated = n_truncated + truncated.shape[0] ### --> hmm how we should classify this??
print(n_truncated)
wt_discar = wt_wt.loc[ wt_wt["Id"].isin( list(as_wt_discarted["V1"]) ) ]
print(n_wt_discart)
n_wt_discart = n_wt_discart + wt_discar.shape[0] ### --> hmm how we should classify this??
print(n_wt_discart)
### Keep track of not yet classified reads
not_classified_yet = sample_results.loc[ ~ sample_results["Id"].isin( checking_tp.Id ) ]

### Classified as wt (by human assesmeny) and not classified as wt (by CRISPR-A)
print("wt just by one of the parts")
hwt_cnwt = sample_results.loc[(sample_results.Classify == "wt") & (sample_results.Nomenclature != "wt")]
print(hwt_cnwt.shape[0])
### Not classified as wt (by human assesmeny); classified as wt (by CRISPR-A)
hnwt_cwt = sample_results.loc[(sample_results.Classify != "wt") & (sample_results.Nomenclature == "wt")]
print(hnwt_cwt.shape[0])

### Assigned as error by human and by CRISPR-A (TN)
human_error = sample_results.loc[(sample_results.Classify == "error")]
n_human_error = human_error.shape[0]
error_initialIndel = human_error.loc[ human_error["Id"].isin( list(initial_edits["Ids"]) ) ]
n_error_initialIndel = error_initialIndel.shape[0]
if(n_human_error == n_error_initialIndel):
    not_rm_error = human_error.loc[ human_error["Id"].isin( list(final_edits["Ids"]) ) ]
    n_not_rm_error = not_rm_error.shape[0]
    n_tn = n_tn + n_error_initialIndel - n_not_rm_error
    n_fp = n_fp + n_not_rm_error
    print("Considered as errors by humans:")
    print(n_not_rm_error) ### False positive
    print(n_error_initialIndel) ### Ture negative
    not_classified_yet = not_classified_yet.loc[ ~ not_classified_yet["Id"].isin( error_initialIndel.Id ) ]
else:
    print("Some of the errors had not been detected as indels by CRISPR-A!")
    print(human_error.loc[ ~ human_error["Id"].isin( list(error_initialIndel["Id"]) ) ])


### Human assigned edit as well as final CRISPR-A edit retrival
human_edit = sample_results.loc[(sample_results.Classify == "edit")] 
n_human_edit = human_edit.shape[0]
print("Human says edit:")
print(n_human_edit)
crispra_edit = human_edit.loc[ human_edit["Id"].isin( list(final_edits["Ids"]) ) ]
n_crispra_edit = crispra_edit.shape[0]
print("Human says edit; as well as CRISPR-A:")
print(n_crispra_edit)
discarted = human_edit.loc[ ~ human_edit["Id"].isin( list(crispra_edit["Id"]) ) ] 
really_discarted = discarted.loc[ ~ discarted["Id"].isin( list(final_edits["Ids"]) ) ]
### Check that there reads have been really discarted
if discarted.shape[0] == really_discarted.shape[0]:
    n_discarted = n_discarted + discarted.shape[0] 
else:
    print("Eps! Check this reads! It seems they where not really discarted")
    print(discarted)
print("This are the edits discarded:")
print(n_discarted) ### Here we have edits discarted
n_tp = n_tp + n_crispra_edit
### Keeping track of not classified reads
not_classified_yet = not_classified_yet.loc[ ~ not_classified_yet["Id"].isin( crispra_edit.Id ) ]
not_classified_yet = not_classified_yet.loc[ ~ not_classified_yet["Id"].isin( discarted.Id ) ]

### Check discarted that has also been discarted by CRISPR-A
discarted_reads = sample_results.loc[(sample_results.Classify == "discarded") & (sample_results.Nomenclature == "discarded")] ## with Classify should be enough!!
### Are these reads saved in truncated or in bad wt? Otherwise they are not aligned or filtered by low read quality
dis_reads_hc = discarted_reads.loc[ discarted_reads["Id"].isin( list(as_aln_discarted["V1"]) ) | discarted_reads["Id"].isin( list(as_wt_discarted["V1"]) )]
print(discarted_reads.shape[0])
print(dis_reads_hc.shape[0])
#print(dis_reads_hc)
### Not aligned or filtered by read quality does not appear here and are directly discarted
n_discarted_reads = n_discarted_reads + discarted_reads.shape[0]
print("Not aligned (inespecific amplification) or reads filtered by low quality:")
print(n_discarted_reads)
### Keeping track of not classified reads
not_classified_yet = not_classified_yet.loc[ ~ not_classified_yet["Id"].isin( discarted_reads.Id ) ]

### Complex indels discarted
# complex_discarted = sample_results.loc[(sample_results.Classify == "complex_indel")].isin( list(as_aln_discarted["V1"] ))
# n_fn = n_fn + complex_discarted.shape[0]

### Error and edits 
edit_error = sample_results.loc[(sample_results.Classify == "edit+error")]
for i in range(0 ,edit_error.shape[0]):
    explored_id = list(edit_error.Id)[i]
    explored_nom = list(edit_error.Nomenclature)[i]
    exp_edit = explored_nom.split("_")[0]
    exp_error = explored_nom.split("_")[1]

    # Check if deletion in edit
    if ( len(exp_edit.split("del")) == 2):
        inied_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "del") & (initial_edits.Start == int(exp_edit.split("del")[0])) & (initial_edits.Length == int(exp_edit.split("del")[1]))]
        fined_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "del") & (final_edits.Start == int(exp_edit.split("del")[0])) & (final_edits.Length == int(exp_edit.split("del")[1]))]
    else:
        inied_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "ins") & (initial_edits.Start == int(exp_edit.split("ins")[0])) & (initial_edits.Length == int(exp_edit.split("ins")[1]))]
        fined_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "ins") & (final_edits.Start == int(exp_edit.split("ins")[0])) & (final_edits.Length == int(exp_edit.split("ins")[1]))]
    # Check if deletion in error
    if ( len(exp_error.split("del")) == 2):
        inier_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "del") & (initial_edits.Start == int(exp_error.split("del")[0])) & (initial_edits.Length == int(exp_error.split("del")[1]))]
        finer_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "del") & (final_edits.Start == int(exp_error.split("del")[0])) & (final_edits.Length == int(exp_error.split("del")[1]))]
    else:
        inier_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "ins") & (initial_edits.Start == int(exp_error.split("ins")[0])) & (initial_edits.Length == int(exp_error.split("ins")[1]))]
        finer_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "ins") & (final_edits.Start == int(exp_error.split("ins")[0])) & (final_edits.Length == int(exp_error.split("ins")[1]))]

    if (inied_check.shape[0] == 1) & (fined_check.shape[0] == 1) & (inier_check.shape[0] == 1) & (finer_check.shape[0] == 0):
        n_tp = n_tp + 1
    elif (fined_check.shape[0] == 0):
        n_discarted = n_discarted + 1
    else:
        print("MEEEG")

### Edits and errors 
error_edit = sample_results.loc[(sample_results.Classify == "error+edit")]
error_edit_2 = error_edit.copy()
for i in range(0 ,error_edit.shape[0]):
    explored_id = list(error_edit.Id)[i]
    explored_nom = list(error_edit.Nomenclature)[i]
    exp_edit = explored_nom.split("_")[1]
    exp_error = explored_nom.split("_")[0]

    # Check if deletion in edit
    if ( len(exp_edit.split("del")) == 2):
        inied_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "del") & (initial_edits.Start == int(exp_edit.split("del")[0])) & (initial_edits.Length == int(exp_edit.split("del")[1]))]
        fined_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "del") & (final_edits.Start == int(exp_edit.split("del")[0])) & (final_edits.Length == int(exp_edit.split("del")[1]))]
    else:
        inied_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "ins") & (initial_edits.Start == int(exp_edit.split("ins")[0])) & (initial_edits.Length == int(exp_edit.split("ins")[1]))]
        fined_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "ins") & (final_edits.Start == int(exp_edit.split("ins")[0])) & (final_edits.Length == int(exp_edit.split("ins")[1]))]
    # Check if deletion in error
    if ( len(exp_error.split("del")) == 2):
        inier_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "del") & (initial_edits.Start == int(exp_error.split("del")[0])) & (initial_edits.Length == int(exp_error.split("del")[1]))]
        finer_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "del") & (final_edits.Start == int(exp_error.split("del")[0])) & (final_edits.Length == int(exp_error.split("del")[1]))]
    else:
        inier_check = initial_edits.loc[ (initial_edits.Ids == explored_id) & (initial_edits.Modification == "ins") & (initial_edits.Start == int(exp_error.split("ins")[0])) & (initial_edits.Length == int(exp_error.split("ins")[1]))]
        finer_check = final_edits.loc[ (final_edits.Ids == explored_id) & (final_edits.Modification == "ins") & (final_edits.Start == int(exp_error.split("ins")[0])) & (final_edits.Length == int(exp_error.split("ins")[1]))]

    if (inied_check.shape[0] == 1) & (fined_check.shape[0] == 1) & (inier_check.shape[0] == 1) & (finer_check.shape[0] == 0):
        n_tp = n_tp + 1
    elif (fined_check.shape[0] == 0):
        n_discarted = n_discarted + 1
    else:
        print("MEEEG")

not_classified_yet = not_classified_yet.loc[ ~ not_classified_yet["Id"].isin( edit_error.Id ) ]
not_classified_yet = not_classified_yet.loc[ ~ not_classified_yet["Id"].isin( error_edit.Id ) ]
not_classified = not_classified_yet.shape[0]

accuracy = (n_tn + n_tp) / (n_tn + n_tp + n_fn + n_fp )
real_edition = (n_tp + n_fn + n_discarted) / (n_tn + n_tp + n_fn + n_fp + n_discarted)

### Final numbers
print( n_tp, n_tn, n_fp, n_fn, not_classified, n_discarted, n_discarted_reads, accuracy, real_edition*100)
### And finally the not classified... 
print(not_classified_yet)

#if ()
#print(human_error.loc[ human_error["Id"].isin( list(initial_edits["Ids"]) ) ])
#print(human_error.loc[ human_error["Id"].isin( list(final_edits["Ids"]) ) ])

# not_wt_wt = sample_results.loc[(sample_results.Classify != "wt") & (sample_results.Nomenclature != "wt")]
# print(not_wt_wt.shape[0])

# print(not_wt_wt)
# true_values = sample_results['Check'].value_counts() 
# bad_results = sample_results.loc[sample_results['Check'] == False] 
# print(bad_results)

#################
##### Notes 
#################
### Should also be descarded -->  SRR3699955.1959      0        error  74del1_77del2_81ins3  