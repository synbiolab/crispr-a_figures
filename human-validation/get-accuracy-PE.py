import numpy as np
import pandas as pd

### Human validated results
sample_results = pd.read_csv("/home/marta/Documents/CRISPR-A/Manuscript-review/Validation-results/SRR10285861_PE_examinated.csv")

# print("Total:")
# print(sample_results.shape[0] )
### Get true negative
wt_wt = sample_results.loc[(sample_results.Classify == "wt") & (sample_results.Nomenclature == "wt")]
#print("TN:")
#print(wt_wt.shape[0])
wt_wt_variant = wt_wt.loc[(sample_results.Variant == "yes")]
wt_wt_endo = wt_wt.loc[(sample_results.Variant == "no")]
### True negative divided by variant
print(wt_wt_variant.shape[0])
print(wt_wt_endo.shape[0])

### Get true positive
tmpl_tmpl = sample_results.loc[(sample_results.Classify == "tmpl") & (sample_results.Nomenclature == "tmpl")]
#print("TP template-based:")
#print(tmpl_tmpl.shape[0])
tmpl_tmpl_variant = tmpl_tmpl.loc[(sample_results.Variant == "yes")]
tmpl_tmpl_endo = tmpl_tmpl.loc[(sample_results.Variant == "no")]
print(tmpl_tmpl_variant.shape[0])
print(tmpl_tmpl_endo.shape[0])
edits = sample_results.loc[(sample_results.Check == 1) & (sample_results.Classify == "edit")]
edits_variant = edits.loc[(edits.Variant == "yes")]
edits_endo = edits.loc[(edits.Variant == "no")]
edits_del = edits.loc[(edits.Variant == "del")]
print(edits_variant.shape[0])
print(edits_endo.shape[0])
print(edits_del.shape[0])

#print("TP indels:")
#print(edits.shape[0])

### Get false negativa 
fn = sample_results.loc[(sample_results.Classify == "edit") & (sample_results.Nomenclature == "wt")]
print("FN:")
print(fn.shape[0])

### Get false positive (indels that are errors)
fp = sample_results.loc[(sample_results.Classify == "error")]
print("FP indels taht are errors:")
print(fp.shape[0])
print(fp)
