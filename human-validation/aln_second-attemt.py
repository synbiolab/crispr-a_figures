from Bio import SeqIO
import pandas as pd
from Bio.Seq import Seq
from itertools import islice

sample_control = {  'SRR3700075' : 'SRR3699789_0.1', 
                    'SRR3699982' : 'SRR3699758_0.1', 
                    'SRR3700021' : 'SRR3699771_0.1', 
                    'SRR3699916' : 'SRR3699736_0.1', 
                    'SRR3699901' : 'SRR3699731_0.1', 
                    'SRR3700042' : 'SRR3699778_0.1' }

sample_reference = {'SRR3700075' : 'CTTGGGGAGAACCATCCTCACCctgctgctgctgctgctgcctggggctagtctcttgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgctgcAGCAGCAGCAAACTGGCGCCGGGAGGTGCTGCGCTCGCGGCCTCTGGGTGCCTGGGGCCCGGGTTCTGGATCACTTCGCGCACGCTCTGGAACAGATTCTGGAAAGCTCCTCGGTAGGTCT',
                    'SRR3699982' : 'ACACTTGGTCCATCCATTTCCAAACCTCCACTGCTGCTCCCGGGTCCTGCTGCCCGAGCCAGGAACTGTGTGTGTTGCAGGGGGGCAGTAACTCCCCAACTCCCTCGTTAATCACAGGATCCCACGAATTTAGGCTcagaagcatcgctcctctcc',
                    'SRR3700021' : 'CCCCTAGTGACTGCCGTCTGCACACCCCGGCTCTGGCTAAAGAGGGAATGGGCTTTGGAAAGGGGGTGGGGGGAGTTTGCTCCTGGACCCCCTATTTCTGACCTCCCAAACAGCTACATATTTGGGACTGGAGTTGCTTCATGTACAGAGAGCCCAGGGCTGGGCACAGGGGCCACAGTGTGTCCCTCTGACAATGTGCCATCTGGAG',
                    'SRR3699916' : 'GTTCTCTGCCGTAGGTGTCCCTTTGAAGGTGCTGGGTGGAGCCCCCCGCTCTGGTGGGTCCTGGTCCCAGTAATAGAGGTTGTCGAAGGCTGGGCTGAAGGCAGGAGGAGGGTGGGGCTGAGGGGCAGCTCCTCCCTGGGGTGTCAAGTACTCGGGGTTCTCCACGGCACCCCCAAAGGCAAAAACGTCTTTGACGACCCCATTCTTCCCTGGGGAGAGAGTCTTGGGCCTTTCCAGAGTGGCACCAGC',
                    'SRR3699901' : 'AGAAGTCCAGCTCCGCACGCGCCAAGCGCTGGGTCTGCAGCTCCCTCGGTTcccgcgcacccaactgccccacgacccctacccgcgcccgcagcccccgcccTCCCGCCCCACTTGcctgggccgccgggcgctggaacctggaccctggaccctggCGGGTTCCGAGCTGCGCCGCCGCCGTCCCTGCCCCTCCAGCACTGGACTCCTCCTTTCCCGTCTTTTTA',
                    'SRR3700042' : 'GGTGTTTCCTGGGGGAAAGTTATGGAAATTCAAAGCATTCTCTTCCTTCCTTACCCGTCTGGATCTTTTTTGGTCCAGTTCGTCAGCACCAAGTCTGAGTGGACCAGGATAGGAGGGAGCCCCAAGTTCCTGGAGACTTCGACAAATGGAAGGGCAAGATTCCTTGGCAGGACCTTTCAGAT' }

orientation_seq = { 'SRR3700075' : "rv" ,
                    'SRR3699982' : "rv" ,
                    'SRR3700021' : "rv" ,
                    'SRR3699916' : "rv" ,
                    'SRR3699901' : "fw" , 
                    'SRR3700042' : "rv"  }

reads_info = pd.read_csv("reads-to-recheck.csv", sep="\t")
samples_names = list(sample_control.keys())

for sample_num in range(0, len(samples_names)):
    crispra_nomenclatures = list()

    # Opening a file to write otput
    s = ""
    outfile_name = s.join( [ "Results_FINAL/", sample_control[samples_names[sample_num]], "_sub-sample_results_RE-DONE.txt" ] )
    results_file = open(outfile_name, 'w')

    ### Get sequence from fastq
    s = ""
    file_name = s.join( [ "Fastqs_FINAL/", sample_control[samples_names[sample_num]], "_sub-sample.fastq" ] )
    record_dict = SeqIO.to_dict(SeqIO.parse(file_name, "fastq"))

    ### Open CRISPR-A info file and get cut site
    indels_reported = pd.read_csv(str("CRISPR-A_ground-results/" + str(sample_control[samples_names[sample_num]]) + "_indels.csv"))
    cut_site = indels_reported.iloc[0].cut_site

    ### Print all alignments of the samples fastq
    reads_for_sample = reads_info.loc[reads_info["Sample"] == samples_names[sample_num]]
    for read_instance in range(0, reads_for_sample.shape[0]):
        ### Get id, sequence and alignment start position
        id_read = reads_for_sample.iloc[read_instance].Read.strip()
        this_seq = record_dict[reads_for_sample.iloc[read_instance].Read.strip()].seq
        cigar = reads_for_sample.iloc[read_instance].CIGAR.split("S")
        cliped_nts = int()
        if len(cigar) > 1:
            try: 
                cliped_nts = int(cigar[0])
            except:
                cliped_nts = 0
        else:
            cliped_nts = 0
        str_position = int(reads_for_sample.iloc[read_instance].Position) - cliped_nts

        ### Flip sequence if needed
        if (orientation_seq[samples_names[sample_num]] == "rv"):
            rev_comp = this_seq.reverse_complement()
        else:
            rev_comp = this_seq
        ### Get CRISPR-A edit information
        reads = indels_reported.loc[(indels_reported["Ids"] == id_read)]

        ### Declare the list for the result
        aln_seq_list = list()

        ### Add "-" when alignment starts after reference beginng
        if str_position > 0:
            for str_pos in range(0, str_position-1):
                aln_seq_list.append("-")
                to_clip = 0
        else: 
            to_clip = str_position*(-1) + 1
            str_position = 0

        ### Add nucleotides that align correctly, insertions and deletions
        if len(aln_seq_list) > 0:
            aln_seq = str(0) + ''.join(aln_seq_list) + str(rev_comp)
        else:
            aln_seq = str(0) + str(rev_comp)

        ### Declare variables
        reads_sorted = reads.sort_values(by='Start')
        k=1
        ins_len = 0
        del_len = 0
        
        ### Get nomenclature of edition
        edit_n = str()
        if reads_sorted.shape[0] == 0:
            crispra_nomenclatures.append("wt")
        else:
            for edit in range(0,reads_sorted.shape[0]-1):
                edit_n = edit_n + str(reads_sorted.iloc[edit].Start) + str(reads_sorted.iloc[edit].Modification) + str(reads_sorted.iloc[edit].Length) + "_"
            final_edit_n = edit_n + str(reads_sorted.iloc[reads_sorted.shape[0]-1].Start) + str(reads_sorted.iloc[reads_sorted.shape[0]-1].Modification) + str(reads_sorted.iloc[reads_sorted.shape[0]-1].Length)
            crispra_nomenclatures.append(final_edit_n)

        # Remove cliped nts
        aln_seq = aln_seq[to_clip:len(aln_seq)]
        ## Incorporate indels into the sequence
        for iteration, i in enumerate(reads_sorted.Start):
            # Loop from the beggining of the sequence to the end
            for j in range(k + str_position, len(aln_seq)*2):
                # Check if we have arrived to a modification
                if (reads_sorted.iloc[iteration].Start == j) and (reads_sorted.iloc[iteration].Modification == 'ins'): 
                    aln_seq_list.append( "[" + str(aln_seq[ j : j+reads_sorted.iloc[iteration].Length ]) + "]" + str(aln_seq[ j+reads_sorted.iloc[iteration].Length : j+reads_sorted.iloc[iteration].Length + 1 ]))
                    k = j + reads_sorted.iloc[iteration].Length + 1 - str_position
                    ins_len = ins_len + reads_sorted.iloc[iteration].Length 
                    break
                elif (reads_sorted.iloc[iteration].Start + ins_len + 1 - del_len == j) and (reads_sorted.iloc[iteration].Modification == 'del'): 
                    aln_seq_list[len(aln_seq_list)-1] = '-'
                    del_len = del_len + reads_sorted.iloc[iteration].Length 
                    if (reads_sorted.iloc[iteration].Length > 1):
                        for del_nt in range(1, reads_sorted.iloc[iteration].Length):
                            aln_seq_list.append('-')
                    k = j - str_position - 1
                    break
                # Get nucleotides before first modification
                if (j >= len(aln_seq)):
                    break
                else:
                    aln_seq_list.append(aln_seq[j]) 

        ## Add nucleotides after the last change
        if ( len(reads_sorted.Start) == 0):
            for j in range(k + str_position, len(aln_seq)):
                aln_seq_list.append(aln_seq[j])
        else:
            missing_part = str(aln_seq)[ len(aln_seq_list) + ins_len - del_len + 1 : len(aln_seq)]
            aln_seq_list = aln_seq_list + list(missing_part)

        result_line = [ crispra_nomenclatures[ read_instance ] ] + aln_seq_list 
        results_file.writelines(str(result_line) + '\n')

    results_file.close()
