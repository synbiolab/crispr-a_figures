from Bio import SeqIO
import pandas as pd
from Bio.Seq import Seq
from itertools import islice

sample_control = { 'SRR10285861' : 'SRR10285861' }
sample_reference = { 'SRR10285861' : 'ATGTGGGCTGCCTAGAAAGGcatggatgagagaagcctggagacagggatcccagggaaacgcccatgcaattagtctatttctgctgcaagtaagcatgcatttgtaggcttgatgctttttttctgcttctccagccctggcctgggtcaatccttggggcccagactgagcacgtgatggcagaggaaaggaagccctgcttcctccagagggcgtcgcaggacagcttttcctagacagggg' }
sample_gRNA = { 'SRR10285861' : 'GGCCCAGACTGAGCACGTGA' }
aln_str = { 'SRR10285861' : 0 }
orientation_seq = { 'SRR10285861' : "fw" }
samples_names = [ 'SRR10285861' ] 
sample_num = 0

read_ids = list()
read_seq = list()
crispra_nomenclatures = list()

# Opening a file to write otput
s = ""
outfile_name = s.join( [ "Results_FINAL/", sample_control[samples_names[sample_num]], "_sub-sample_results.txt" ] )
results_file = open(outfile_name, 'w')

### Get sequence from fastq
s = ""
file_name = s.join( [ "Fastqs_FINAL/", sample_control[samples_names[sample_num]], "_1_trimmed_subset.fastq" ] )
with open(file_name) as handle:
    for record in SeqIO.parse(handle, "fastq"):
        read_ids.append(record.id)
        read_seq.append(record.seq)

### Open CRISPR-A info file and get cut site
indels_reported = pd.read_csv(str("CRISPR-A_ground-results/" + str(sample_control[samples_names[sample_num]]) + "_indels.csv"))
cut_site = indels_reported.iloc[0].cut_site

### Print sample reference
ref_sequence = list(sample_reference[samples_names[sample_num]])
print(sample_reference[samples_names[sample_num]])
ref_sequence[cut_site-1] = ref_sequence[cut_site-1] + "|"
ref_line = ["Reference"] + ref_sequence
results_file.writelines(str(ref_line) + '\n')

### Print all alignments of the samples fastq
for read_instance in range(0, len(read_ids)):
    id_read = read_ids[read_instance] 
    this_seq = read_seq[read_instance]
    ### Flip sequence if needed
    if (orientation_seq[samples_names[sample_num]] == "rv"):
        rev_comp = this_seq.reverse_complement()
    else:
        rev_comp = this_seq
    ### Get CRISPR-A edit information
    reads = indels_reported.loc[(indels_reported["Ids"] == id_read)]

    ### Declare the list for the result
    aln_seq_list = list()

    ### Add "-" when alignment starts after reference beginng
    for str_pos in range(0,aln_str[samples_names[sample_num]]):
        aln_seq_list.append("-")

    ### Add nucleotides that align correctly, insertions and deletions
    if len(aln_seq_list) > 0:
        aln_seq = str(0) + ''.join(aln_seq_list) + str(rev_comp)
    else:
        aln_seq = str(0) + str(rev_comp)

    ### Declare variables
    reads_sorted = reads.sort_values(by='Start')
    k=1
    ins_len = 0
    del_len = 0
    
    ### Get nomenclature of edition
    edit_n = str()
    if reads_sorted.shape[0] == 0:
        crispra_nomenclatures.append("wt")
    else:
        for edit in range(0,reads_sorted.shape[0]-1):
            edit_n = edit_n + str(reads_sorted.iloc[edit].Start) + str(reads_sorted.iloc[edit].Modification) + str(reads_sorted.iloc[edit].Length) + "_"
        final_edit_n = edit_n + str(reads_sorted.iloc[reads_sorted.shape[0]-1].Start) + str(reads_sorted.iloc[reads_sorted.shape[0]-1].Modification) + str(reads_sorted.iloc[reads_sorted.shape[0]-1].Length)
        crispra_nomenclatures.append(final_edit_n)

    ### Incorporate indels into the sequence
    for iteration, i in enumerate(reads_sorted.Start):
        # Loop from the beggining of the sequence to the end
        for j in range(k + aln_str[samples_names[sample_num]], len(aln_seq)*2):
            # Check if we have arrived to a modification
            if (reads_sorted.iloc[iteration].Start == j) and (reads_sorted.iloc[iteration].Modification == 'ins'): 
                aln_seq_list.append( "[" + str(aln_seq[ j : j+reads_sorted.iloc[iteration].Length ]) + "]" + str(aln_seq[ j+reads_sorted.iloc[iteration].Length : j+reads_sorted.iloc[iteration].Length + 1 ]))
                k = j + reads_sorted.iloc[iteration].Length + 1 - aln_str[samples_names[sample_num]]
                ins_len = ins_len + reads_sorted.iloc[iteration].Length 
                break
            elif (reads_sorted.iloc[iteration].Start + ins_len + 1 - del_len == j) and (reads_sorted.iloc[iteration].Modification == 'del'): 
                aln_seq_list[len(aln_seq_list)-1] = '-'
                del_len = del_len + reads_sorted.iloc[iteration].Length 
                if (reads_sorted.iloc[iteration].Length > 1):
                    for del_nt in range(1, reads_sorted.iloc[iteration].Length):
                        aln_seq_list.append('-')
                k = j - aln_str[samples_names[sample_num]] - 1
                break
            # Get nucleotides before first modification
            if (j >= len(aln_seq)):
                break
            else:
                aln_seq_list.append(aln_seq[j]) 

    ### Add nucleotides after the last change
    if ( len(reads_sorted.Start) == 0):
        for j in range(k + aln_str[samples_names[sample_num]], len(aln_seq)):
            aln_seq_list.append(aln_seq[j])
    else:
        missing_part = str(aln_seq)[ len(aln_seq_list) + ins_len - del_len + 1 : len(aln_seq)]
        aln_seq_list = aln_seq_list + list(missing_part)

    result_line = [ crispra_nomenclatures[ read_instance ] ] + aln_seq_list 
    results_file.writelines(str(result_line) + '\n')

results_file.close()