
# CRISPR-A Figures
These scripts allow to reproduce the figures crafted for CRISPR-A paper available at:

You can find the tool at:

## How this repo is organised?
The folder tree is the following

- [SimGE-validation-data](#SimGE-validation-data) - 
  - [Real](#Real) - 
  - [Simulated](#Simulated) -
- [preprocessed-data](#preprocessed-data) - 
  - [Analysis_cell-lines_results](#Analysis_cell-lines_results) - 
- [preprocessing-scripts](#preprocessing-scripts) - 
- [01_Figure-2_indels.R](#01_Figure-2_indels.R) - 
- [02_Figure-3_template-based.R](#02_Figure-3_template-based.R) -
- [03_Figure-4_cell-lines.R](#03_Figure-4_cell-lines.R) - 
- [04_Figure-1-SimGE.R](#04_Figure-1-SimGE.R) -
- [05_Figure-5_precision.R](#05_Figure-5_precision.R) -
- [06_Figure-5_mock_compare.R](#06_Figure-5_mock_compare.R) - 
- [07_Figures-supplementary.R](#07_Figures-supplementary.R) - 
- [08_DE_HCT116-k562.R](#08_DE_HCT116-k562.R) - 
- [Performance_assesment.Rmd](#Performance_assesment.Rmd) - 
- [noise_exploration.Rmd](#noise_exploration.Rmd) - 
- [tool_comparation_noiseSubstraction.Rmd](#tool_comparation_noiseSubstraction.Rmd) - 


## Description
This repository contain all scripts that have been used to generate CRISPR-A manuscript figures. In addition, it also contains scripts for the pre-processing of the data to lately do the plots. 

### SimGE-validation-data
Data used to build the heatmap to compare editing results samples with the simulations obtained with the same target and cut site.   
#### Real
Editing results.
#### Simulated
Simulated results. 

### preprocessed-data
All data used to get the plots. 
#### Analysis_cell-lines_result
Gene editing results. There are all the indel outcomes and its characteristics as well as the gRNA sequences.

### preprocessing-scripts
Scripts used to process data and get the data that is used to get the plots.

### 01_Figure-2_indels.R
Get plots for Figure 2: Indels characterization algorithm development and benchmark with simulated data and real data.

### 02_Figure-3_template-based.R
Get plots for Figure 3: Template-based edits and substitutions characterization benchmark with simulated data and real data.

### 03_Figure-4_cell-lines.R
Get plots for Figure 4: HCT116, HEK-293, and K562 edited in 96 different targets analyzed by CRISPR-A.

### 04_Figure-1-SimGE.R
Get plots for Figure 1:  CRISPR-A capabilities for analysis and simulation of CRISPR-based experiments.

### 05_Figure-5_precision.R
Get plots for Figure 5: Enhanced precision with spikes and UMIs characterization

### 06_Figure-5_mock_compare.R
Get plots for Figure 5: Enhanced precision with mock characterization

### 07_Figures-supplementary.R
Get plots for Supplementary Figures.

### 08_DE_HCT116-k562.R
Differential Expression analysis of HCT116 and K562 cell lines.

### Performance_assesment.Rmd

In this file, we obtained the statistical data of all the comparisons between Treated vs mock files. For this purpose, the algorithm that performs this comparison has been used in different scenarios (without normalizing and normalizing) to see whether there was any improvement. After obtaining the data, you will find in the plots where this statistical information is shown, such as in which position more noise is found, and how much of this noise is being corrected. This gives us information about our algorithm's specificity and performance within the framework of our analysis.

### noise_exploration.Rmd
In order to be able to compare mock files with treated files, we wanted to explore if there were any characteristics significantly different between both populations. This is important to have then into account when comparing to not bias the correction. You will find here the plots together with the statistical analysis for features like distance from the cut site, size and indel length

### tool_comparation_noiseSubstraction.Rmd

Here we gathered the data from the different tools that have a mock-based correction of noise, to compare them for the 864 samples that we used in our study. We computed the amount of correction by subtracting the percentage of correction before and after the correction. The results are represented in a plot. 
