#!/usr/bin/python3
import csv
from sys import argv

with open('temples_info.csv', newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter=',')
	header = True
	for row in reader:
		##### Info from sample
		if header:
			header=False
		elif row[0] == argv[1]:
			count = 1
			for i in range(1100):
				identifier = '@om_'+str(count) 
				readSeq = row[8]
				i = "I"*len(readSeq)
				count = count +1
				print(identifier)
				print(readSeq)
				print('+')
				print(i)

