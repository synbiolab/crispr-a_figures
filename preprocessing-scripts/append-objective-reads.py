#!/usr/bin/python3

from Bio import SeqIO
import csv
from random import sample
from random import choices
from Bio.Seq import Seq


##### Variables to take into account to generate diverse templates
nts = ['A', 'C', 'T', 'G']
change = ['subs', 'del', 'ins', 'delins']
size = list(range(1, 16)) # from 1 to 15
arm_len = list(range(15, 91)) #for left as well as for rigth arm
direction = ['rv', 'fw']
cut_dist = list(range(9))

info = list()
info.append(['id', 'change', 'left_arm', 'cut_site_dist', 'rigth_arm', 'mod_size', 'direction', 'template', 'read'])

with open('sequences_cut-sites.csv', newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter=',')
	header = True
	for row in reader:
		##### Info from sample
		if header:
			header=False
		else:
			##### Samples information
			a = row

			##### Random sampling
			left_start = 0
			right_end = 0
			while left_start <= 0 or right_end <= 0:
				c = sample(change,1)[0]
				s = sample(size,1)[0]
				s2 = sample(size,1)[0]
				al = sample(arm_len,1)[0] 
				ar = sample(arm_len,1)[0]
				d = sample(direction,1)[0]
				ct = sample(cut_dist,1)[0]

				##### Get template and read sequence 
				left_start = int(a[1])-al-ct
				right_end = int(a[1])-ct+ar

			if c == 'subs':
				subs_nts = ''.join(choices(nts, k=s))
				template = a[4][left_start:int(a[1])-ct] + subs_nts + a[4][int(a[1])-ct+s:right_end+s]
				read = a[4][0:int(a[1])-ct] + subs_nts + a[4][int(a[1])-ct+s:len(a[4])]
			elif c == 'ins':
				ins_nts = ''.join(choices(nts, k=s))
				template = a[4][left_start:int(a[1])-ct] + ins_nts + a[4][int(a[1])-ct:right_end+s]
				read = a[4][0:int(a[1])-ct] + ins_nts + a[4][int(a[1])-ct:len(a[4])]
			elif c == 'del':
				template = a[4][left_start:int(a[1])-ct] + a[4][int(a[1])-ct+s:right_end+s]
				read = a[4][0:int(a[1])-ct] + a[4][int(a[1])-ct+s:len(a[4])]
			else:
				ins_nts = ''.join(choices(nts, k=s2))
				template = a[4][left_start:int(a[1])-ct] + ins_nts + a[4][int(a[1])-ct+s:right_end+s]
				read = a[4][0:int(a[1])-ct] + ins_nts + a[4][int(a[1])-ct+s:len(a[4])]

			if d == 'rv':
				seq = Seq(template)
				template = str(seq.reverse_complement())
			
			info.append([a[3],c,al,ct,ar,s,d,template, read.upper()])


with open("temples_info.csv", "w") as f:
    writer = csv.writer(f)
    writer.writerows(info)


